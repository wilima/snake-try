function love.load()
    scale = love.window.getPixelScale( )
    player = {}
    player.x = 1
    player.y = 1
    player.moving = {}
    player.moving.actual = "right"
    player.moving.next = "right"

    grid = {}
    grid.squareSize = 20 * scale
    grid.size = 20
    grid.board = {}

    for row = 1, grid.size, 1 do 
        grid.board[row] = {}
        for column = 1, grid.size, 1 do
            grid.board[row][column] = {}
            grid.board[row][column].x = row * grid.squareSize
            grid.board[row][column].y = column * grid.squareSize
        end
    end  


    printVal = false
    debug = "nothing"

    time = 0
end

function love.update(dt)
    time = time + dt
    if time > 0.2 then
        player.x = player.x + 1
        player.y = player.y
        time  = 0
    end
end

function love.draw()
    love.graphics.rectangle("fill", grid.board[player.x][player.y].x, grid.board[player.x][player.y].y, grid.squareSize, grid.squareSize)
end

function love.keypressed(key)
   if (key == "up" or "down" or "left" or "right") then
        player.moving.next = key
   end
end


